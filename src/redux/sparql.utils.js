export const sendQuery = async (query) => {
    const url = new URL("https://data.escr.fr/sparql");
    const params = {
        query: query
    };
    url.search = new URLSearchParams(params)
        .toString();
    const myHeaders = new Headers();
    myHeaders.append("Accept", "application/sparql-results+json");
    const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    const result = await fetch(url, requestOptions)
        .then(response => {
            return response.json()
        })
        .catch(error => this.displayError(error));
    return result.results;
}