export const getAmountShip = (datas) => {
    return datas
      .bindings
      .filter(x => x.p.value.split("#").pop() === 'nombreNavire')[0].v.value;
  }
  
  export const getEmission = (datas) => {
    return datas
      .bindings
      .filter(x => x.p.value.split("#").pop() === 'emission')[0].v.value;
  }