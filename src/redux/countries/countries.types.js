const CountriesActionTypes = {
    SET_NAME: 'SET_NAME',
    SET_DATA: 'SET_DATA',
}

export default CountriesActionTypes;