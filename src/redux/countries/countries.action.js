import CountriesActionTypes from './countries.types';
import {
  sendQuery
} from '../sparql.utils'

import {
  getAllCountryQuery,
  queryDataPollutionBasedCountry,
  getCountryCoordinates
} from './query.utils'

import {
  getAmountShip,
  getEmission,
} from './countries.utils'

export const setName = name => ({
  type: CountriesActionTypes.SET_NAME,
  payload: name
});

export const setData = data => ({
  type: CountriesActionTypes.SET_DATA,
  payload: data
});


export const fetchAllCountry = () => {
  const data = []
  return async (dispatch) => {
    return sendQuery(getAllCountryQuery).then(response => {
      response.bindings.forEach(async bs => {
        const elemUrl = bs.s.value;
        const temp = elemUrl.split('/');
        const name = temp[temp.length - 1];
        fetchDataPollution(name).then(res => {
          data.push(res)
        })
      });
      
      dispatch(setData(data))
    })
  }
}



export const fetchDataPollution = async (nameCountry) => {
  const query = queryDataPollutionBasedCountry(nameCountry);
  const pollutionDatas = await sendQuery(query);
  const amountShip = getAmountShip(pollutionDatas)
  const emission = getEmission(pollutionDatas)
  const coordinates = getCountryCoordinates(nameCountry)
  
  return {
    name: nameCountry,
    amountShip: amountShip,
    emission: emission,
    coordinates: coordinates
  }
}