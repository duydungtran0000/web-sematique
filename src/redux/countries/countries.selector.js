import {
    createSelector
} from 'reselect';

export const selectCountries = state => state.countries;

export const selectCountryNames = createSelector(
    [selectCountries],
    countries => countries.name
)

export const selectPollutionDatas = createSelector(
    [selectCountries],
    countries => countries.data
)

export const selectIsLoading = createSelector(
    [selectCountries],
    countries => countries.isLoading
)