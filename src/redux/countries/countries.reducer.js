import CountryActionTypes from './countries.types';
const INITIAL_STATE = {
    name: null,
    data: null,
    isLoading: true
}

const countriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CountryActionTypes.SET_NAME:
            return {
                ...state,
                name: action.payload
            }
        case CountryActionTypes.SET_DATA:
            return {
                ...state,
                data: action.payload,
                isLoading: false
            }
        default:
            return state
    }
}

export default countriesReducer