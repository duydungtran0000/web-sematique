export const getAllCountryQuery = `
BASE <https://data.escr.fr/wiki/Utilisateur:Tdzung0000/projet#>
SELECT * 
  WHERE {
  ?s a <Pay> .
}`;

export const queryDataPollutionBasedCountry = (country) => {
  const request = `
    BASE <https://data.escr.fr/wiki/Utilisateur:Tdzung0000/projet#>
    PREFIX ex: <http://www.example.org/>
    SELECT * 
    WHERE {
    ex:${country} ?p ?v .
    }
  `
  return request 
}

export const getCountryCoordinates = (country) => {
  switch(country) {
      case "France": return [2, 46]
      case "Italy": return [12, 43]
      case "Espagne": return [-3, 39]
      case "Greece": return [23, 39]
      case "Norway": return [8, 60]
      default : return null
  }
}