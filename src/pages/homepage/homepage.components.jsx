import React from "react";
import "./homepage.styles.scss";
import { connect } from "react-redux";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import { fetchAllCountry } from "../../redux/countries/countries.action";

import { createStructuredSelector } from "reselect";
import { selectIsLoading } from "../../redux/countries/countries.selector";

import MapChart from "../../components/map-chart/map-chart.components";
import RankTable from "../../components/rank-table/rank-table.component";

import WithSpinner from "../../components/with-spinner/with-spinner.component";

const MapChartWithSpinner = WithSpinner(MapChart);
const RankTableWithSpinner = WithSpinner(RankTable);

class Homepage extends React.Component {
  componentDidMount() {
    this.props.fetchAllCountry();
  }

  state = {
    option: "so2",
  };

  handleChange = (event) => {
    this.setState({
      option: event.target.value,
    });
  };

  render() {
    return (
      <div>
        <FormControl component="fieldset">
          <FormLabel component="legend">Select option</FormLabel>
          <RadioGroup
            row
            aria-label="position"
            name="position"
            defaultValue="top"
          >
            <FormControlLabel
              value="so2"
              checked={this.state.option === "so2"}
              onChange={this.handleChange}
              control={<Radio color="primary" />}
              label="SO2 (Unit: Ton of SO2)"
            />
            <FormControlLabel
              value="amount"
              checked={this.state.option === "amount"}
              onChange={this.handleChange}
              control={<Radio color="primary" />}
              label="Amount Ship"
            />
          </RadioGroup>
        </FormControl>
        <div class="content">
          <div>
            <MapChartWithSpinner
              isLoading={this.props.isLoading}
              option={this.state.option}
            />
          </div>
          <div>
            <RankTableWithSpinner
              isLoading={this.props.isLoading}
              propOption={this.state.option}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchAllCountry: () => dispatch(fetchAllCountry()),
});

const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
