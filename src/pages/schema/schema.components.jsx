import React from "react";
import schema from "../../assets/img/schema.png";
import './schema.style.scss'

const SchemaPage = () => {
  return (
    <div>
      <h1>Schema Page</h1>
      <div className="img-content">
        <img alt="schema" src={schema} />
      </div>
    </div>
  );
};

export default SchemaPage;
