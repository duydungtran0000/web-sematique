import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { createStructuredSelector } from "reselect";
import { selectPollutionDatas } from "../../redux/countries/countries.selector";
import { connect } from "react-redux";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const RankTable = ({ propOption, pollutionData }) => {
  const classes = useStyles();
  const [option, setOption] = useState(propOption);
  // const [data, setData] = useState(pollutionData);

  useEffect(() => {
    setOption(propOption);
    if (option === "so2") {
      pollutionData.sort(function (a, b) {
        return parseInt(b.emission) - parseInt(a.emission);
      });
      console.log(pollutionData);
    } else {
      pollutionData.sort(function (a, b) {
        return parseInt(b.amountShip) - parseInt(a.amountShip);
      });
      console.log(pollutionData);
    }
  }, [propOption]);

  return (
    <div>
      <h4>Countries in Europe most polluted by ships (data collected 2017)</h4>
      <TableContainer component={Paper}>
        <Table
          className={classes.table}
          size="small"
          aria-label="a dense table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Country</TableCell>
              {option === "so2" ? (
                <TableCell align="right">Emission (SO2)</TableCell>
              ) : (
                <TableCell align="right">
                  Total number of ships annually
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {pollutionData ? (
              pollutionData.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  {option === "so2" ? (
                    <TableCell align="right">{row.emission}</TableCell>
                  ) : (
                    <TableCell align="right">{row.amountShip}</TableCell>
                  )}
                </TableRow>
              ))
            ) : (
              <h1>Loading</h1>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  pollutionData: selectPollutionDatas,
});

export default connect(mapStateToProps)(RankTable);
