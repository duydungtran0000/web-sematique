export const makers = [
    {
      name: "France",
      coordinates: [2, 46],
      amountShip: 5
    },
    {
      name: "Italy",
      coordinates: [12, 43],
      amountShip: 5
    },
    {
      name: "Spain",
      coordinates: [-3, 39],
      amountShip: 5
    },
    {
      name: "Greece",
      coordinates: [23, 39],
      amountShip: 5
    },
    {
      name: "Norway",
      coordinates: [8, 60],
      amountShip: 5
    },
  ]