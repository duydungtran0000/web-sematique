import React from "react";
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  ZoomableGroup,
} from "react-simple-maps";
import "./map-chart.styles.scss";
import { createStructuredSelector } from "reselect";
import { selectPollutionDatas } from "../../redux/countries/countries.selector";
import { connect } from "react-redux";

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

class MapChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = { w: 800, h: 600 };
  }

  render() {
    return (
      <div>
        <ComposableMap
          projection="geoMercator"
          projectionConfig={{
            center: [13, 52],
            translate: [this.state.w / 2, this.state.h / 2],
            scale: this.state.w / 1.5,
          }}
          style={{ height: this.state.h, width: this.state.w }}
        >
          <ZoomableGroup zoom={1}>
            <Geographies geography={geoUrl}>
              {({ geographies }) =>
                geographies.map((geo) => (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    fill="#DDD"
                    stroke="#FFF"
                  />
                ))
              }
            </Geographies>
            {this.props.pollutionData ? (
              this.props.pollutionData.map(
                ({ name, coordinates, amountShip, emission }) => (
                  <Marker key={name} coordinates={coordinates}>
                    {this.props.option === "so2" ? (
                      <text
                        dx="-3.2"
                        dy="3"
                        style={{ fontFamily: "system-ui", fill: "#F53" }}
                      >
                        {emission}
                      </text>
                    ) : (
                      <g>
                        <circle
                          r={12}
                          fill="#F00"
                          stroke="#fff"
                          strokeWidth={2}
                        ></circle>
                        <text
                          dx="-3.2"
                          dy="3"
                          style={{ fontFamily: "system-ui", fill: "#FFF" }}
                        >
                          {amountShip}
                        </text>
                      </g>
                    )}

                    <text
                      textAnchor="middle"
                      y={-15}
                      style={{ fontFamily: "system-ui", fill: "#F53" }}
                    >
                      {name}
                    </text>
                  </Marker>
                )
              )
            ) : (
              <h1>loadding</h1>
            )}
          </ZoomableGroup>
        </ComposableMap>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  pollutionData: selectPollutionDatas,
});

export default connect(mapStateToProps)(MapChart);
