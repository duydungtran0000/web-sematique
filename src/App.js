import './App.css';
import Homepage from './pages/homepage/homepage.components';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from './components/drawer/drawer.component';
import { Route, Switch, Redirect } from "react-router-dom";
import SchemaPage from './pages/schema/schema.components'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const App = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Drawer/>
        <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route path='/schema' component={SchemaPage}/>
          <Route path="" component={Homepage}/>
          <Redirect to="" />
        </Switch>   
        </main>
    </div>
  );
}

export default App;
